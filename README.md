# Webarchitects Mastodon Ansible role

This is based on [the install instructions](https://docs.joinmastodon.org/admin/install/), see the [mastodon-server repo](https://git.coop/webarch/mastodon-server) for an example of how to use this role to configure a server.

## Defaults

| Variable name             | Default value              | Comment                                              |
|---------------------------|----------------------------|------------------------------------------------------|
| `mastodon`                | `false`                    |                                                      |
| `mastodon_domain`         | `{{ inventory_hostname }}` |                                                      |
| `mastodon_rails_env`      | `production`               | Value for the RAILS_ENV environment variable         |
| `mastodon_group`          | `mastodon`                 |                                                      |
| `mastodon_ruby_version`   | `3.0.3`                    |                                                      |
| `mastodon_user`           | `mastodon`                 |                                                      |
| `mastodon_version`        | `latest`                   | Use a version number or "latest"                     |

## Manual steps

Currently these manual steps are required after the PostgreSQL database has been created:

```bash
su - postgres
psql postgres
```

```sql
ALTER USER mastodon CREATEDB;
UPDATE pg_database SET datistemplate = FALSE WHERE datname = 'template1';
DROP DATABASE template1;
CREATE DATABASE template1 WITH TEMPLATE = template0 ENCODING = 'UNICODE';
UPDATE pg_database SET datistemplate = TRUE WHERE datname = 'template1';
\c template1
VACUUM FREEZE;
```

The run the interactive setup wizard as the `mastodon` user:

```bash
RAILS_ENV=production bundle exec rake mastodon:setup
```

Then add the following to `/home/mastodon/live/.env.production`:

```ini
EMAIL_DOMAIN_ALLOWLIST=example.org|example.com
```

For [unencrypted email](https://github.com/mastodon/mastodon/issues/772) via `localhost` edit `/home/mastodon/live/.env.production`:

```ini
```

Then restart the services:

```bash
service nginx restart
service mastodon-sidekiq restart
service mastodon-streaming restart
service mastodon-web restart
```
